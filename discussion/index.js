console.log(`1. drink html`);
console.log(`2. eat javascript`);
console.log(`3. inhale bootstrap`);
console.log(`4. bake bootstrap`);

let tasks = ["drink html", "eat javascript", "inhale css", "bake bootstrap"];
console.log(tasks);
console.log(tasks[2]);
// console.log(tasks[4]);
// console.log(tasks[2].length);
console.log(tasks.length);


// Array Manipulation

// Add an element

let numbers = ["one", "two", "three", "four"];
console.log(numbers);

// assigning
numbers[4] = "five";
console.log(numbers);


// Push Method
numbers.push("element");
console.log(numbers);

    //callback function
    function pushMethod(element){
        numbers.push(element);
    }

    pushMethod("six");
    console.log(numbers);

// remove (last)
numbers.pop();
console.log(numbers);

// remove (first)
numbers.shift();
console.log(numbers);

// callback function
function shiftMethod(){
    numbers.shift();
}
shiftMethod();
console.log(numbers);

// ADD am element
numbers.unshift("zero");
console.log(numbers);

function unshiftMethod(element){
    numbers.unshift(element)
}

unshiftMethod("mcdo");
console.log(numbers);

// arrangement of the element

let numbs = [15, 27, 32, 12, 6, 8, 236]
// sort method
numbs.sort(
    function(a,b){
        // a is for the smallest value
        // b is for the largest value
        return a-b
    }
)
console.log(numbs);

// reverse the arrangement
numbs.reverse()
console.log(numbs)

// spliced method
let nums = numbs.splice(4,2,31,11, 111);
// (/* first parameter indicared what index to start on */ /* second parameter indicates how many indexes */)
console.log(numbs);
console.log(nums);


// slice method

let slicedNums = numbs.slice(4,6);
console.log(numbs);
console.log(slicedNums);

// merging of array
// concat

console.log(numbers);
console.log(numbs);
let animals = ["dog", "tiger", "kangaroo", "chicken"]
console.log(animals);

let newConcat = numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);


// join method

let meal = ["rice", "steak", "juice"]
console.log(meal);
let newJoin = meal.join();
console.log(newJoin);


 newJoin = meal.join("");
console.log(newJoin);

 newJoin = meal.join(" ");
console.log(newJoin);

 newJoin = meal.join("-");
console.log(newJoin);

// merging
// tostring method

console.log(nums);
console.log(typeof nums);

let newString = numbs.toString();
console.log(typeof newString);

/* Accessors */
let countries = ["US", "PH", "JP", "HK", "SG", "PH", "NZ"];
// Index of - only checks the index of the first element that matches
let index = countries.indexOf("PH");
console.log(`index of`);
console.log(index);



// lastIndexOf - checks the last index of the last element that matches
 index = countries.lastIndexOf("PH");
 console.log("reverse index")
console.log(index);


/* if (countries.indexOf("CAN") === -1) {
	console.log("Element not existing");
}else{
	console.log("Element exists in the array");
} */


// iterators
// forEach

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
console.log(days);


// foreach
days.forEach(
    function(element){
        console.log(element);
    }
)


// map
let mapDays = days.map(
    function(element){
        return `${element} is the day of the week`
    }
)

console.log(mapDays);
console.log(days);


let newFilter = numbs.filter(
    function(element){
        return element<30   
    }
)
console.log(numbs)
console.log(newFilter);

// includes
let animalIncludes = animals.includes("dog");
console.log(animalIncludes);
console.log(animals);


// every   
let newEvery = nums.every(
    function(element){
        return (element>5);
    }
)
console.log(nums);
console.log(`new every ${newEvery}`);


// some
let newSome = nums.some(
    function(element){
        return ( element > 10 );
    }
)
console.log(newSome);



// reduce
let newReduce = nums.reduce(
    function (a,b){
        return a + b
    }
)
console.log(newReduce);

let average = newReduce/nums.length
console.log(average);


// toFixed
console.log(average.toFixed(2));


console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));