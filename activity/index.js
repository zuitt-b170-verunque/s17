// console.log(`Hello World`);
let students = [];

// callback function for adding student
function addStudent(element){
    students.push(element);
    console.log(`${element} was added to the students list`);
}


// callback function for counting student
function countStudent(){
    console.log(`There are a total of ${students.length} students`);
}

// sort and foreach callback function
function printStudent(){
    
    // sort
    students.sort(
        function(a,b){
            return a-b
        }
    )

    // foreach
    students.forEach(
        function(element){
            console.log(element);
        }
    )
}


// callback function for finding student



let students2 = ["David", "John", "Dominic", "Jesus", "Gardo"]
function findStudent(arr, query){
    arr.filter(function(el) {
         el.toLowerCase().indexOf(query.toLowerCase()) !== -1 
         console.log(`${findStudent(students, query)} are enrollees`)   
      }
    
      )
}
/**
 * Filter array items based on search criteria (query)
 */
function filterItems(arr, query) {
  return arr.filter(function(el) {
    return el.toLowerCase().indexOf(query.toLowerCase()) !== -1
    
  }
  )
}

// multiple students
console.log(`${filterItems(students2, 'd')} are enrollees`) 

// singe student
console.log(`${filterItems(students2, 'Gardo')} is an enrollee`)

// Does not exist
console.log(`${filterItems(students2, 'q')} is not enrolled`)


console.log(`callback functions : addStudent, countStudent, printStudent`)

